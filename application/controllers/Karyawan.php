<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function form()
	{
		$footer = [
			'js' => ''
		];

		$this->template([], 'karyawan/form', [], $footer);
	}

}

/* End of file Karyawan.php */
/* Location: ./application/controllers/Karyawan.php */ ?>