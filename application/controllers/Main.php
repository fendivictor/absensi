<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function menu()
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->Menu_Model->create_array(0);

			echo json_encode($data);
		}
	}
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */ ?>