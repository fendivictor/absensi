<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function users()
	{
		$this->template([], 'management/users');
	}
}

/* End of file Management.php */
/* Location: ./application/controllers/Management.php */ ?>