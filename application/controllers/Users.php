<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function data()
	{
		$this->template([], 'users/data', [], []);
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */ ?>