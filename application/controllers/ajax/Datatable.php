<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datatable extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (! $this->input->is_ajax_request()) {
			show_404();
		}

		$this->load->library('datatables');
		$this->load->model('User_Model');
	}

	public function dt_user()
	{
		$data = $this->User_Model->dt_user();
		$this->output->set_content_type('application/json')->set_output($data);
	}

}

/* End of file Datatables.php */
/* Location: ./application/controllers/Datatables.php */ ?>