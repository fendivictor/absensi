<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function template($header_data = [], $content = '', $body_data = [], $footer_data = [])
	{
		$menu = $this->Menu_Model->create_menu(0);
		$page_title = $this->Menu_Model->current_menu()->label;

		$header_data['menu'] = $menu;
		$header_data['page_title'] = $page_title;

		$this->load->view('template/header', $header_data);
		$this->load->view($content, $body_data);
		$this->load->view('template/footer', $footer_data);
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */
?>