<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function menu($parent = 0)
	{
		return 
			$this->db->query(
				"	SELECT *
					FROM ms_menu a
					LEFT JOIN (
						SELECT parent, COUNT(id) AS jumlah
						FROM ms_menu
						WHERE `status` = 1
						GROUP BY parent
					) AS b ON a.id = b.parent
					WHERE a.`status` = 1
					AND a.`parent` = '$parent'
					ORDER BY a.`urutan` ")->result();	
	}

	public function current_menu()
	{
		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

		return
			$this->db->where([
				'fungsi' => $class, 
				'method' => $method
			])
				->get('ms_menu')
				->row();
	}

	public function create_array($parent = 0)
	{
		$menus = $this->menu($parent);

		$data = [];
		if ($menus) {
			foreach ($menus as $menu => $mn) {
				$data[$menu] = [
					'id' => $mn->id,
					'label' => $mn->label,
					'url' => $mn->url,
					'icon' => $mn->icon,
					'fungsi' => $mn->fungsi,
					'method' => $mn->method,
					'parent' => $mn->parent
				];

				if ($mn->jumlah > 0) {
					$submenus = $this->menu($mn->parent);

					if ($submenus) {
						foreach ($submenus as $submenu) {
							$data[$menu]['child'][] = [
								'id' => $submenu->id,
								'label' => $submenu->label,
								'url' => $submenu->url,
								'icon' => $submenu->icon,
								'fungsi' => $submenu->fungsi,
								'method' => $submenu->method,
								'parent' => $submenu->parent
							];
						}
					}
				}
			}
		}

		return $data;
	}

	public function create_menu($parent = 0)
	{
		$menus = $this->create_array(0);
		$current_menu = $this->current_menu();

		$element = '';
		if ($menus) {
			foreach ($menus as $menu) {
				$child = isset($menu['child']) ? $menu['child'] : [];

				if (count($child) > 0) {
					$active = ($current_menu->parent == $menu['id']) ? 'active' : '';

					$element .= '<li class="pcoded-hasmenu '.$active.'">
                                        <a href="javascript:void(0)" class="waves-effect waves-dark">
                                            <span class="pcoded-micon">
                                                <i class="feather '.$menu['icon'].'"></i>
                                            </span>
                                            <span class="pcoded-mtext">'.$menu['label'].'</span>
                                        </a>
                                        <ul class="pcoded-submenu">';

                    foreach ($child as $submenu) {
                        $element .= '<li class="">
                                        <a href="'.base_url().$submenu['url'].'" class="waves-effect waves-dark">
                                            <span class="pcoded-mtext">'.$submenu['label'].'</span>
                                        </a>
                                    </li>';
                    }

                    $element .=  '</ul></li>';
				} else {
					$active = ($current_menu->id == $menu['id']) ? 'active' : '';
					$element .= '
						<li class="'.$active.'">
							<a href="'.base_url().$menu['url'].'" class="waves-effect waves-dark">
								<span class="pcoded-micon">
                                    <i class="feather '.$menu['icon'].'"></i>
                                </span>
                                <span class="pcoded-mtext">'.$menu['label'].'</span>
                            </a>
                        </li>';
				}
			}
		}

		return $element;
	}
}

/* End of file Menu_Model.php */
/* Location: ./application/models/Menu_Model.php */ ?>