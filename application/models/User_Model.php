<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function dt_user()
	{
		$this->datatables->select("(@rownum := @rownum + 1) AS num, a.profile_name, a.email, a.no_telp, a.jenis_kelamin");
		$this->datatables->add_column('action', '');
		$this->datatables->from('tb_user a, (SELECT @rownum := 0) AS b');

		return $this->datatables->generate();
	}
}

/* End of file User_Model.php */
/* Location: ./application/models/User_Model.php */ ?>