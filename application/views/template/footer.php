                </div>
            </div>
        </div>
    </div>
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade
            <br/>to any of the following web browsers to access this website.
        </p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="../files/assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="../files/assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="../files/assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/jquery.min.js"></script>
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/jquery-ui.min.js"></script>
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/popper.min.js"></script>
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/bootstrap.min.js"></script>
    <script src="<?= base_url('assets') ?>/js/waves.min.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url('assets') ?>/js/jquery.flot.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/jquery.flot.categories.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/curvedlines.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/jquery.flot.tooltip.min.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/amcharts.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/serial.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/light.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/markerclusterer.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/pcoded.min.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script src="<?= base_url('assets') ?>/js/vertical-layout.min.js" type="2d8d78e876b340f9029c575b-text/javascript"></script>
    <script type="2d8d78e876b340f9029c575b-text/javascript" src="<?= base_url('assets') ?>/js/script.min.js"></script>
    <script src="<?= base_url('assets') ?>/js/rocket-loader.min.js" data-cf-settings="2d8d78e876b340f9029c575b-|49" defer=""></script>

    <script>
        const baseUrl = '<?= base_url() ?>';

        const blockUI = () => {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        }

        const unBlockUI = () => {
            $.unblockUI();
        }
    </script>
    
    <?php  
        if ($js) {
            foreach ($js as $js) {
                echo '<script src="'.base_url($js).'"></script>';
            } 
        }
    ?>

    </body>
</html>