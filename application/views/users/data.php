<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Simple panel -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Registered Users</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-hovered" id="dt-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>No HP</th>
                                            <th>JenKel</th>
                                            <th>Tools</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /simple panel -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->

<script>
    var table = $("#dt-table").DataTable({
        serverSide: true,
        ajax: {
            url: baseUrl + 'ajax/Datatable/dt_user',
            type: 'post'
        },
        column: [
            {data: 'num'},
            {data: 'profile_name'},
            {data: 'email'},
            {data: 'no_telp'},
            {data: 'jenis_kelamin'},
            {data: 'action'}
        ]
    });
</script>