/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.38-MariaDB : Database - absensi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ms_admin` */

DROP TABLE IF EXISTS `ms_admin`;

CREATE TABLE `ms_admin` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT '',
  `password` varchar(250) DEFAULT '',
  `status` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_user` varchar(60) DEFAULT '',
  `updated_user` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ms_admin` */

/*Table structure for table `ms_menu` */

DROP TABLE IF EXISTS `ms_menu`;

CREATE TABLE `ms_menu` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(60) DEFAULT '',
  `url` varchar(60) DEFAULT '',
  `icon` varchar(20) DEFAULT '',
  `fungsi` varchar(20) DEFAULT '',
  `method` varchar(20) DEFAULT '',
  `parent` int(20) DEFAULT '0',
  `urutan` int(10) DEFAULT '0',
  `status` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `ms_menu` */

insert  into `ms_menu`(`id`,`label`,`url`,`icon`,`fungsi`,`method`,`parent`,`urutan`,`status`,`created_at`,`updated_at`) values (1,'Dashboard','Dashboard/index','icon-home','Dashboard','index',0,1,1,'2020-05-10 19:48:45',NULL),(2,'Karyawan','','icon-user','','',0,2,1,'2020-05-10 19:49:01',NULL),(3,'Tambah Karyawan','Karyawan/form','','Karyawan','form',2,1,1,'2020-05-10 19:49:25',NULL),(4,'Karyawan Active','Karyawan/active','','Karyawan','active',2,2,1,'2020-05-10 19:49:45',NULL),(5,'Karyawan Resign','Karyawan/resign','','Karyawan','resign',2,3,1,'2020-05-10 19:50:03',NULL),(6,'Report Karyawan','Karyawan/report','','Karyawan','report',2,4,1,'2020-05-10 19:50:20',NULL),(7,'Absensi','','icon-calendar','','',0,3,1,'2020-05-10 19:50:34',NULL),(8,'Data Presensi','Presensi/index','','Presensi','index',7,1,1,'2020-05-10 19:50:57','2020-05-10 19:52:37'),(9,'Data Absensi','Absensi/index','','Absensi','index',7,2,1,'2020-05-10 19:51:10','2020-05-10 19:52:41'),(10,'Data Terlambat','Terlambat/index','','Terlambat','index',7,3,1,'2020-05-10 19:51:24','2020-05-10 19:52:45'),(11,'Report Absensi','Absensi/report','','Absensi','report',7,4,1,'2020-05-10 19:51:39','2020-05-10 19:52:48'),(12,'Penjadwalan','','icon-calendar','','',0,4,1,'2020-05-10 19:52:08',NULL),(13,'Data Jadwal','Jadwal/index','','Jadwal','index',12,1,1,'2020-05-10 19:52:32','2020-05-10 19:53:18'),(14,'Jadwal Karyawan','Jadwal/karyawan','','Jadwal','karyawan',12,2,1,'2020-05-10 19:53:12','2020-05-10 19:53:19'),(15,'Settings','Settings/index','icon-settings','Settings','index',0,5,1,'2020-05-10 19:53:45',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
